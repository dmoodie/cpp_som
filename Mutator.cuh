#pragma once
#include "Helpers.hpp"
#include "Defines.hpp"
#include "Distance.cuh"
#include <opencv2/core/cuda_types.hpp>
#include <opencv2/core/cuda.hpp>


template<typename T, typename MUTATOR>
__global__ void mutate_som_kernel(
	cv::cuda::PtrStepSz<T> node_weights,
	const som_idx_type SOM_size,
	cv::cuda::PtrStepSz<T> input,
	cv::cuda::PtrStepSz<int> BMU,
	cv::cuda::PtrStepSz<T> accumulated_change,
	int input_index,
	som_idx_type step,
	MUTATOR mutator)
{
	som_idx_type node_id;
	int index = *BMU.ptr(input_index);
	if (index >= node_weights.rows)
	{
		printf("%d Invalid index (%d) out of range (%d) \n", __LINE__, index, node_weights.rows);
		return;
	}
	if (index == -1)
		return;
	som_idx_type BMU_index = GetIndex((size_t)index, SOM_size);

	node_id.x = threadIdx.x + BMU_index.x;
	node_id.y = threadIdx.y + BMU_index.y;
	node_id.z = threadIdx.z + BMU_index.z;

	if (node_id.x < step.x)
		return;
	if (node_id.y < step.y)
		return;
	if (node_id.z < step.z)
		return;
	node_id.x -= step.x;
	node_id.y -= step.y;
	node_id.z -= step.z;
	if (node_id.x > SOM_size.x ||
		node_id.y > SOM_size.y ||
		node_id.z > SOM_size.z)
		return;

	if (step.w > BMU_index.w)
	{
		node_id.w = 0;
	}
	else
	{
		node_id.w = BMU_index.w - step.w;
	}
	T change = mutator(node_weights, BMU_index, node_id, SOM_size);

	if (!isnan(change) && !isinf(change))
		atomicAdd(accumulated_change.ptr(), change);
}



class Mutator
{
protected:
	int neighborhood_size;
	som_idx_type som_size;

public:
	Mutator() : neighborhood_size(5){ som_size.x = 0; som_size.y = 0; som_size.z = 0; som_size.w = 0; }
	virtual void Mutate(cv::cuda::GpuMat& node_weights, cv::cuda::GpuMat& BMU_idx, cv::cuda::GpuMat& inputs, cv::cuda::GpuMat& accumulated_change, int idx, cv::cuda::Stream stream) = 0;
	virtual void Mutate(cv::cuda::GpuMat& node_weights, cv::cuda::GpuMat& BMU_idx, cv::cuda::GpuMat& inputs, cv::cuda::GpuMat& accumulated_change, cv::cuda::Stream stream) = 0;
	virtual void SetNeighborhoodSize(int newSize){	neighborhood_size = newSize;	}
	virtual void SetSomSize(som_idx_type size){ som_size = size; }
};


// This class encapsulates the launch and running of the mutate kernel
template<typename T,typename MUTATE_OP>
class Mutator_impl: public Mutator
{
	MUTATE_OP op;
public:
	Mutator_impl(MUTATE_OP op_) :op(op_){}

	virtual void Mutate(cv::cuda::GpuMat& node_weights, cv::cuda::GpuMat& BMU_idx, cv::cuda::GpuMat& inputs,cv::cuda::GpuMat& accumulated_change, int idx, cv::cuda::Stream stream)
	{
		CV_Assert(node_weights.depth() == CV_TYPE<T>::DEPTH);
		if (accumulated_change.size() != cv::Size(1,1) || accumulated_change.depth() != node_weights.depth())
			accumulated_change.create(1, 1, node_weights.depth());
		dim3 threads;
		threads.x = Mutator::neighborhood_size; // std::min((unsigned int)neighborhood_size, som_size.x);
		threads.y = Mutator::neighborhood_size; // std::min((unsigned int)neighborhood_size, som_size.y);
		threads.z = Mutator::neighborhood_size; // std::min((unsigned int)neighborhood_size, som_size.z);
		som_idx_type step;
		step.x = (threads.x - 1) / 2;
		step.y = (threads.y - 1) / 2;
		step.z = (threads.z - 1) / 2;
		step.w = (std::min((unsigned int)Mutator::neighborhood_size, som_size.w) - 1) / 2;
		
		op.op.SetInput(cv::cuda::PtrStepSz<T>(inputs.row(idx)));
		//std::cout << "Blocks: " << blocks.x << "," << blocks.y << "," << blocks.z << " steps: " << step.x << "," << step.y << "," << step.z << "," << step.w << std::endl;
		mutate_som_kernel<T, MUTATE_OP > << <1, threads, 0, cv::cuda::StreamAccessor::getStream(stream) >> >(
			cv::cuda::PtrStepSz<T>(node_weights),
			Mutator::som_size,
			cv::cuda::PtrStepSz<T>(inputs),
			cv::cuda::PtrStepSz<int>(BMU_idx),
			cv::cuda::PtrStepSz<T>(accumulated_change),
			idx,
			step,
			op);
	}
	virtual void Mutate(cv::cuda::GpuMat& node_weights, cv::cuda::GpuMat& BMU_idx, cv::cuda::GpuMat& inputs, cv::cuda::GpuMat& accumulated_change, cv::cuda::Stream stream)
	{
		for (int i = 0; i < inputs.rows; ++i)
		{
			Mutate(node_weights, BMU_idx, inputs,accumulated_change, i, stream);
		}
	}
};

template<typename T, template<typename>class DIST_OP>
class default_mutate_op
{
	T alpha;

public:
	Dist_base<T, DIST_OP> op;
	default_mutate_op(const T alpha_, const Dist_base<T, DIST_OP> op_) : alpha(alpha_), op(op_){}
	default_mutate_op(const T alpha_) : alpha(alpha_){}

	__device__ __host__ T operator()(cv::cuda::PtrStepSz<T> node_weights, const som_idx_type bmu, const som_idx_type idx, const som_idx_type som_size)
	{
		// Index of node to be mutated
		auto index = GetIndex(idx, som_size);
		if (index >= node_weights.rows)
		{
			printf("%lld Index (%lld) > node_weights.rows (%lld) : %lld,%lld,%lld,%lld\n", __LINE__, index, node_weights.rows, bmu.x, bmu.y, bmu.z, bmu.w);
			GetIndex(idx, som_size);
			return 0;
		}
		auto node_ptr = node_weights.ptr(index);
		auto input_ptr = op.data.ptr();
		// Get distance between this node and the bmu
		auto som_dist = op(bmu, idx);
		// Get distance between this node and the input vector
		auto input_dist = op(node_ptr);
		T change = 0.0;
		for (int i = 0; i < node_weights.cols; ++i)
		{
			T delta = alpha *  input_dist * (input_ptr[i] - node_ptr[i]) / (1 + som_dist);
			atomicAdd(node_ptr + i, delta);
			change += delta;
		}
		return change;
	}
};