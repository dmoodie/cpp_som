#pragma once

#include "Defines.hpp"
#include "Helpers.hpp"
#include <opencv2/core/cuda_types.hpp>


template<typename T, template<typename> class IMPL>
struct Dist_base: public IMPL<T>
{
	cv::cuda::PtrStepSz<T> data;
	Dist_base(){}
	Dist_base(const cv::cuda::PtrStepSz<T>& data_) : data(data_){}
	void SetInput(const cv::cuda::PtrStepSz<T>& data_){ data = data_; }

	// Returns the euclidean distance between a node at node_id and data
	__device__ __host__ T operator()(const cv::cuda::PtrStepSz<T> nodes, som_idx_type node_id, som_idx_type som_size)
	{
		auto index = GetIndex(node_id, som_size);
		auto ptr1 = nodes.ptr(index);
		return operator()(ptr1);
	}
	__device__ __host__ T operator()(T* ptr1)
	{
		T distance = 0;
		auto ptr2 = data.ptr();
		for (int i = 0; i < data.cols; ++i)
		{
			distance += IMPL<T>::operator()(ptr1, ptr2, i);
		}
		return distance;
	}
	__device__ __host__ T operator()(const som_idx_type idx1, const som_idx_type idx2)
	{
		return IMPL<T>::operator()(idx1, idx2);
	}
};

// Distance calculation implementations
template<typename T>
struct dist_L2
{	
	__device__ __host__ T operator()(T* ptr1, T* ptr2, int i)
	{
		return (ptr1[i] - ptr2[i])*(ptr1[i] - ptr2[i]);
	}

	__device__ __host__ T operator()(const som_idx_type idx1, const som_idx_type idx2)
	{
		return (idx1.x - idx2.x)*(idx1.x - idx2.x)+
			   (idx1.y - idx2.y)*(idx1.y - idx2.y)+
			   (idx1.z - idx2.z)*(idx1.z - idx2.z)+
 			   (idx1.w - idx2.w)*(idx1.w - idx2.w);
	}
};

template<typename T>
struct dist_L1
{

};