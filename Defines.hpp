#pragma once
#include <cuda.h>
#include <cuda_runtime.h>
#include <opencv2/core/cvdef.h>

typedef uint4 som_idx_type;
typedef unsigned int idx_type;

template<typename T> struct
CV_TYPE
{
	static const int DEPTH;
};

template<> static const int CV_TYPE<float>::DEPTH = CV_32F;
template<> static const int CV_TYPE<double>::DEPTH = CV_64F;
template<> static const int CV_TYPE<int>::DEPTH = CV_32S;
template<> static const int CV_TYPE<uchar>::DEPTH = CV_8U;
template<> static const int CV_TYPE<char>::DEPTH = CV_8S;
template<> static const int CV_TYPE<ushort>::DEPTH = CV_16U;
template<> static const int CV_TYPE<short>::DEPTH = CV_16S;


