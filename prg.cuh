#pragma once

struct prg
{
	float a, b;
	size_t offset;
	__host__ __device__
		prg(float _a = 0.f, float _b = 1.f, size_t offset_ = 0) : a(_a), b(_b), offset(offset_) {};

	__host__ __device__
		float operator()(const unsigned int n) const
	{
		thrust::default_random_engine rng;
		thrust::uniform_real_distribution<float> dist(a, b);
		rng.discard(n + offset);

		return dist(rng);
	}
};