#pragma once
#include "Defines.hpp"


template<typename idx_type, typename size_type> __device__ __host__ size_t GetIndex(idx_type idx, size_type size)
{
	return size.x * size.y * size.z * idx.w +
		size.x * size.y * idx.z +
		size.x * idx.y +
		idx.x;
}

template<typename size_type> __device__ __host__ som_idx_type GetIndex(size_t idx, size_type size)
{
	som_idx_type index;
	index.w = idx / (size.x * size.y * size.z);
	idx -= index.w * size.x * size.y * size.z;
	index.z = idx / (size.x * size.y);
	idx -= index.z * size.x * size.y;
	index.y = idx / (size.x);
	idx -= index.y * size.x;
	index.x = idx % (size.w * size.y * size.z);
	return index;
}