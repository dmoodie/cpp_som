#include <opencv2/core/cuda.hpp>

namespace cv
{
	namespace cuda
	{
		void SVD(GpuMat input, GpuMat& U, GpuMat& V, GpuMat& S, cv::cuda::Stream stream = Stream::Null());
		void QR(GpuMat input, GpuMat& Q, GpuMat* R = nullptr, cv::cuda::Stream stream = Stream::Null());
		void hconcat(GpuMat left, GpuMat right, GpuMat& dest, int d_type = -1, Stream stream = Stream::Null());
		void vconcat(GpuMat top, GpuMat bottom, GpuMat& dest, int d_type = -1, Stream stream = Stream::Null());
	}
}

class iterative_svd
{
public:
	iterative_svd();

	void initialize(cv::cuda::GpuMat X, cv::cuda::GpuMat& U, cv::cuda::GpuMat& S, cv::cuda::GpuMat& V, cv::cuda::Stream stream = cv::cuda::Stream::Null());
	void setRank(int r);

	void addRows(cv::cuda::GpuMat X, cv::cuda::GpuMat& U, cv::cuda::GpuMat& S, cv::cuda::GpuMat& V, cv::cuda::Stream stream = cv::cuda::Stream::Null());
	void addCols(cv::cuda::GpuMat X, cv::cuda::GpuMat& U, cv::cuda::GpuMat& S, cv::cuda::GpuMat& V, cv::cuda::Stream stream = cv::cuda::Stream::Null());


private:
	cv::cuda::GpuMat _U, _V, _S;
	cv::Size x_size;
	int rank;
};