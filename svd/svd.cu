#include "svd.h"
#include <opencv2/cudaarithm.hpp>
#include <cusolverDn.h>
#include <opencv2/core/cuda_stream_accessor.hpp>
#include "../Thrust_interop.hpp"
#include <thrust/system/cuda/execution_policy.h>
static const char *_cusolverGetErrorEnum(cusolverStatus_t error)
{
	switch (error)
	{
	case CUSOLVER_STATUS_SUCCESS:
		return "CUSOLVER_SUCCESS";

	case CUSOLVER_STATUS_NOT_INITIALIZED:
		return "CUSOLVER_STATUS_NOT_INITIALIZED";

	case CUSOLVER_STATUS_ALLOC_FAILED:
		return "CUSOLVER_STATUS_ALLOC_FAILED";

	case CUSOLVER_STATUS_INVALID_VALUE:
		return "CUSOLVER_STATUS_INVALID_VALUE";

	case CUSOLVER_STATUS_ARCH_MISMATCH:
		return "CUSOLVER_STATUS_ARCH_MISMATCH";

	case CUSOLVER_STATUS_EXECUTION_FAILED:
		return "CUSOLVER_STATUS_EXECUTION_FAILED";

	case CUSOLVER_STATUS_INTERNAL_ERROR:
		return "CUSOLVER_STATUS_INTERNAL_ERROR";

	case CUSOLVER_STATUS_MATRIX_TYPE_NOT_SUPPORTED:
		return "CUSOLVER_STATUS_MATRIX_TYPE_NOT_SUPPORTED";

	}

	return "<unknown>";
}
inline void __cusolveSafeCall(cusolverStatus_t err, const char *file, const int line)
{
	if (CUSOLVER_STATUS_SUCCESS != err) {
		fprintf(stderr, "CUSOLVE error in file '%s', line %d\n %s\nerror %d: %s\nterminating!\n", __FILE__, __LINE__, err, \
			_cusolverGetErrorEnum(err)); \
			cudaDeviceReset(); assert(0); \
	}
}

#define cusolveSafeCall(call) __cusolveSafeCall(call, __FILE__, __LINE__)

namespace cv
{
	namespace cuda
	{
		void SVD(GpuMat input, GpuMat& U, GpuMat& V, GpuMat& S, cv::cuda::Stream stream)
		{
			CV_Assert(input.type() == CV_32F || input.type() == CV_32FC2 || input.type() == CV_64F || input.type() == CV_64FC2);
			CV_Assert(input.isContinuous());
			int M = input.rows;
			int N = input.cols;

			//auto startAllocation = clock();
			if (U.size() != cv::Size(M, M) || U.type() != input.type())
				U = createContinuous(M, M, input.type());
			if (V.size() != cv::Size(N, N) || V.type() != input.type())
				V = createContinuous(N, N, input.type());
			if (S.size() != cv::Size(1, N) || S.depth() != input.type())
				S = createContinuous(1, N, input.depth());

			static cusolverDnHandle_t solver_handle = NULL;
			if (!solver_handle)
				cusolverDnCreate(&solver_handle);

			int work_size = 0;

			cusolverStatus_t stat;
			switch (input.type())
			{
			case CV_32F:   stat = cusolverDnSgesvd_bufferSize(solver_handle, M, N, &work_size); break;
			case CV_64F:   stat = cusolverDnDgesvd_bufferSize(solver_handle, M, N, &work_size); break;
			case CV_32FC2: stat = cusolverDnCgesvd_bufferSize(solver_handle, M, N, &work_size); break;
			case CV_64FC2: stat = cusolverDnZgesvd_bufferSize(solver_handle, M, N, &work_size); break;
			}
			stat = cusolverDnSgesvd_bufferSize(solver_handle, M, N, &work_size);

			//if (stat != CUSOLVER_STATUS_SUCCESS) std::cout << "Initialization of cuSolver failed. \n";
			CV_Assert(stat == CUSOLVER_STATUS_SUCCESS && "Initialization of cuSolver failed");

#ifdef __OPENCV_BUILD
			BufferPool workPool(stream);
			auto workBuffer = workPool.getBuffer(1, work_size, input.type());
			auto devInfo = workPool.getBuffer(1, 1, CV_32S);
#else
			auto workBuffer = createContinuous(1, work_size, input.type());
			auto devInfo = createContinuous(1, 1, CV_32S);
#endif

			/*
			Setting the stream of the solver wont make the actual call return immediately since there is interaction between the CPU and GPU during that call.  Instead it will make sure other code doesn't block due to
			the solver running on the null stream.
			*/
			cusolverDnSetStream(solver_handle, cv::cuda::StreamAccessor::getStream(stream));
			switch (input.type())
			{
			case CV_32F:
				stat = cusolverDnSgesvd(solver_handle, 'A', 'A', M, N, input.ptr<float>(), M, S.ptr<float>(), U.ptr<float>(), M, V.ptr<float>(), N, workBuffer.ptr<float>(), work_size, NULL, devInfo.ptr<int>()); break;
			case CV_64F:
				stat = cusolverDnDgesvd(solver_handle, 'A', 'A', M, N, input.ptr<double>(), M, S.ptr<double>(), U.ptr<double>(), M, V.ptr<double>(), N, workBuffer.ptr<double>(), work_size, NULL, devInfo.ptr<int>());	break;
			case CV_32FC2:
				stat = cusolverDnCgesvd(solver_handle, 'A', 'A', M, N, input.ptr<cuComplex>(), M, S.ptr<float>(), U.ptr<cuComplex>(), M, V.ptr<cuComplex>(), N, workBuffer.ptr<cuComplex>(), work_size, NULL, devInfo.ptr<int>());	break;
			case CV_64FC2:
				stat = cusolverDnZgesvd(solver_handle, 'A', 'A', M, N, input.ptr<cuDoubleComplex>(), M, S.ptr<double>(), U.ptr<cuDoubleComplex>(), M, V.ptr<cuDoubleComplex>(), N, workBuffer.ptr<cuDoubleComplex>(), work_size, NULL, devInfo.ptr<int>());	break;
			default:
				CV_Assert(false && "Not a valid datatype");
			}

			switch (stat)
			{
			case CUSOLVER_STATUS_NOT_INITIALIZED:   CV_Assert(false && "Library cuSolver not initialized correctly");	break;
			case CUSOLVER_STATUS_INVALID_VALUE:     CV_Assert(false && "Invalid parameters passed");                    break;
			case CUSOLVER_STATUS_INTERNAL_ERROR:    CV_Assert(false && "Internal operation failed");                    break;
			}
			if (!stream)
				cudaDeviceSynchronize();
			//std::cout << "End time " << clock() << "\n";
		}

		// **************************************************************************************
		// ************************** QR factorization ******************************************
		// **************************************************************************************
		// http://stackoverflow.com/questions/22399794/qr-decomposition-to-solve-linear-systems-in-cuda
		void QR(GpuMat input_, GpuMat& Q, GpuMat* R, cv::cuda::Stream stream)
		{
			GpuMat input;
			if (R == nullptr)
			{
				input = input_;
			}
			else
			{
				input = createContinuous(input_.size(), input_.type());
				input_.copyTo(input, stream);
			}
			CV_Assert(input.isContinuous());
			static cusolverDnHandle_t solver_handle = NULL;
			if (!solver_handle)
				cusolverDnCreate(&solver_handle);
			cusolverStatus_t cusolver_status = CUSOLVER_STATUS_SUCCESS;
			int m = input.rows;
			int n = input.cols;
			int lda = m;
			int lwork = 0;
			
			switch (input.type())
			{
			case CV_32F:
				cusolver_status = cusolverDnSgeqrf_bufferSize(solver_handle, m, n, input.ptr<float>(), lda, &lwork);
				break;
			default:
				CV_Assert(false && "Unacceptable input type");
				
			}

			CV_Assert(cusolver_status == CUSOLVER_STATUS_SUCCESS && "Error calculating work buffer size");

			cv::cuda::GpuMat tau(1, std::min(m,n), input.type());
			cv::cuda::GpuMat work_buffer(1, lwork, input.type());
			cv::cuda::GpuMat dev_info(1, 1, CV_32S);
			switch (input.type())
			{
			case CV_32F:
				cusolver_status = cusolverDnSgeqrf(solver_handle, m, n, input.ptr<float>(), m, tau.ptr<float>(), work_buffer.ptr<float>(), lwork, dev_info.ptr<int>());
				break;
			default:
				CV_Assert(false && "Unacceptable input type");
			}
			if (cusolver_status != CUSOLVER_STATUS_SUCCESS)
			{
				std::cout << _cusolverGetErrorEnum(cusolver_status);
			}
			cusolveSafeCall(cusolver_status);
			cv::cuda::GpuMat d_Q = createContinuous(m, m, input.type());
			d_Q.setTo(cv::Scalar(0), stream);
			thrust::fill(thrust::system::cuda::par.on(cv::cuda::StreamAccessor::getStream(stream)), GpuMatDiagBeginItr<float>(d_Q), GpuMatDiagEndItr<float>(d_Q), 1);
			
			
			switch (input.type())
			{
			case CV_32F:
				cusolverDnSormqr(
					solver_handle, 
					CUBLAS_SIDE_LEFT, 
					CUBLAS_OP_N, 
					m, 
					n, 
					std::min(m, n), 
					input.ptr<float>(), 
					m, 
					tau.ptr<float>(), 
					d_Q.ptr<float>(), 
					n, 
					work_buffer.ptr<float>(), 
					lwork, 
					dev_info.ptr<int>());
				break;
			}
			if (cusolver_status != CUSOLVER_STATUS_SUCCESS)
			{
				std::cout << _cusolverGetErrorEnum(cusolver_status);
			}
			if (R)
			{
				*R = input;
			}
			Q = d_Q;
		}
	
	
		
		void hconcat(GpuMat left, GpuMat right, GpuMat& dest, int d_type, Stream stream)
		{
			CV_Assert(left.rows == right.rows);
			Size result_size(left.cols + right.cols, left.rows);
			if (d_type == -1)
			{
				CV_Assert(left.type() == right.type());
				d_type = left.type();
			}
			if (dest.size() != result_size)
				dest = createContinuous(result_size, d_type);
			left.copyTo(dest(Rect(0, 0, left.cols, left.rows)), stream);
			right.copyTo(dest(Rect(left.cols, 0, right.cols, right.rows)), stream);
		}

		void vconcat(GpuMat top, GpuMat bottom, GpuMat& dest, int d_type, Stream stream)
		{
			CV_Assert(top.cols == bottom.cols);
			Size result_size(top.cols, top.rows + bottom.rows);
			if (d_type == -1)
			{
				CV_Assert(top.type() == bottom.type());
				d_type = top.type();
			}
			if (dest.size() != result_size)
				dest = createContinuous(result_size, d_type);
			top.copyTo(dest(Rect(0, 0, top.cols, top.rows)), stream);
			bottom.copyTo(dest(Rect(0, top.rows, bottom.cols, bottom.rows)), stream);
		}
	}
}

iterative_svd::iterative_svd() :rank(0)
{
	
}
void iterative_svd::initialize(cv::cuda::GpuMat X, cv::cuda::GpuMat& U, cv::cuda::GpuMat& S, cv::cuda::GpuMat& V, cv::cuda::Stream stream)
{
	cv::cuda::SVD(X, _U, _V, _S, stream);
	U = _U;
	V = _V;
	S = _S;
	setRank(S.cols);
	
}
void iterative_svd::setRank(int r)
{
	rank = r;
	_S = _S.colRange(0, rank);
	_U = _U.colRange(0, rank);
	_V = _V.rowRange(0, rank);
}

void iterative_svd::addRows(cv::cuda::GpuMat X, cv::cuda::GpuMat& U, cv::cuda::GpuMat& S, cv::cuda::GpuMat& V, cv::cuda::Stream stream)
{

}

void iterative_svd::addCols(cv::cuda::GpuMat C, cv::cuda::GpuMat& U, cv::cuda::GpuMat& S, cv::cuda::GpuMat& V, cv::cuda::Stream stream)
{
	CV_Assert(C.type() == _U.type());
	cv::cuda::GpuMat L, H, J, K, Q, u, s, v;
	
	cv::cuda::gemm(_U, C, 1, cv::noArray(), 0, L, cv::GEMM_1_T, stream); // L = transpose(U)*C
	//H = cv::cuda::createContinuous(_U.rows, L.cols, C.type());
	cv::cuda::gemm(_U, L, -1, C, 1, H, 0, stream); // H = C - U*L
	cv::cuda::QR(H, J, &K, stream);
	cv::Size Q_size;
	Q_size.width = rank;
	Q_size.height = rank;
	Q_size.width += L.cols;
	Q_size.height += K.rows;
	Q = cv::cuda::createContinuous(Q_size, C.type());
	Q.setTo(cv::Scalar(0), stream);
	// Populate the diagonal of Q with S
	switch (C.type())
	{
	case CV_32F:
	{
		thrust::copy(thrust::system::cuda::par.on(cv::cuda::StreamAccessor::getStream(stream)), GpuMatBeginItr<float>(_S), GpuMatEndItr<float>(_S), GpuMatDiagBeginItr<float>(Q));
		break;
	}
	case CV_32FC2:
	{
		thrust::copy(thrust::system::cuda::par.on(cv::cuda::StreamAccessor::getStream(stream)), GpuMatBeginItr<float>(_S), GpuMatEndItr<float>(_S), GpuMatDiagBeginItr<float>(Q));
		thrust::copy(thrust::system::cuda::par.on(cv::cuda::StreamAccessor::getStream(stream)), GpuMatBeginItr<float>(_S,1), GpuMatEndItr<float>(_S,1), GpuMatDiagBeginItr<float>(Q,1));
		break;
	}
	case CV_64F:
	{
		thrust::copy(thrust::system::cuda::par.on(cv::cuda::StreamAccessor::getStream(stream)), GpuMatBeginItr<double>(_S), GpuMatEndItr<double>(_S), GpuMatDiagBeginItr<double>(Q));
		break;
	}
	case CV_64FC2:
	{
		thrust::copy(thrust::system::cuda::par.on(cv::cuda::StreamAccessor::getStream(stream)), GpuMatBeginItr<double>(_S), GpuMatEndItr<double>(_S), GpuMatDiagBeginItr<double>(Q));
		thrust::copy(thrust::system::cuda::par.on(cv::cuda::StreamAccessor::getStream(stream)), GpuMatBeginItr<double>(_S, 1), GpuMatEndItr<double>(_S, 1), GpuMatDiagBeginItr<double>(Q, 1));
		break;
	}
	default:
		return;
	}

	// Populate L
	L.copyTo(Q(cv::Rect(rank, 0, L.cols, L.rows)), stream);
	K.copyTo(Q(cv::Rect(rank, rank, K.cols, K.rows)), stream);
	// Calculate incremental svd
	cv::cuda::SVD(Q, u, v, s, stream);
	// Calculate the updated svd
	// It would seem I messed this part up, need to correct the matrix
	cv::cuda::GpuMat tmpU = cv::cuda::createContinuous(u.rows, u.cols + J.cols, C.type());
	u.copyTo(tmpU(cv::Rect(0, 0, u.cols, u.rows)), stream);
	J.copyTo(tmpU(cv::Rect(U.cols, 0, J.cols, J.rows)), stream);
	// U'' = [U J]*U'
	cv::cuda::gemm(_U, tmpU, 1, cv::noArray(), 0, _U, 0, stream);

	// V'' = [V 0; 0 I] V'
	cv::cuda::GpuMat tmpV = cv::cuda::createContinuous(v.size(), C.type());
	tmpV.setTo(cv::Scalar(0), stream);
	_V.copyTo(tmpV(cv::Rect(0, 0, _V.cols, _V.rows)), stream);
	cv::cuda::GpuMat I = tmpV(cv::Rect(_V.cols, _V.rows, tmpV.cols - _V.cols, tmpV.rows - _V.rows));
	switch (C.type())
	{
		case CV_32F:
		{
			thrust::fill(thrust::system::cuda::par.on(cv::cuda::StreamAccessor::getStream(stream)), GpuMatDiagBeginItr<float>(I), GpuMatDiagEndItr<float>(I), 1);
			break;
		}
		case CV_32FC2:
		{
			thrust::fill(thrust::system::cuda::par.on(cv::cuda::StreamAccessor::getStream(stream)), GpuMatDiagBeginItr<float>(I), GpuMatDiagEndItr<float>(I), 1);
			thrust::fill(thrust::system::cuda::par.on(cv::cuda::StreamAccessor::getStream(stream)), GpuMatDiagBeginItr<float>(I,1), GpuMatDiagEndItr<float>(I,1), 1);
			break;
		}
		case CV_64F:
		{
			thrust::fill(thrust::system::cuda::par.on(cv::cuda::StreamAccessor::getStream(stream)), GpuMatDiagBeginItr<double>(I), GpuMatDiagEndItr<double>(I), 1);
			break;
		}
		case CV_64FC2:
		{
			thrust::fill(thrust::system::cuda::par.on(cv::cuda::StreamAccessor::getStream(stream)), GpuMatDiagBeginItr<double>(I), GpuMatDiagEndItr<double>(I), 1);
			thrust::fill(thrust::system::cuda::par.on(cv::cuda::StreamAccessor::getStream(stream)), GpuMatDiagBeginItr<double>(I,1), GpuMatDiagEndItr<double>(I,1), 1);
			break;
		}
	}
	
	cv::cuda::gemm(tmpV, v, 1.0, cv::noArray(), 0, _V, 0, stream);
	_S = s;
	S = s;
	U = _U;
	V = _V;
}


