#include <thrust/system/cuda/execution_policy.h>
#include <opencv2/core/cuda_stream_accessor.hpp>
#include <opencv2/cudaarithm.hpp>
#include <thrust/transform.h>
#include <thrust/random.h>
#include <thrust/extrema.h>
#include "Thrust_interop.hpp"

#define FLANN_USE_CUDA
#include <flann/flann.hpp>
#include <flann/util/DeviceMatrix.h>
#include <flann/algorithms/kdtree_cuda_index.cuh>



struct prg
{
	float a, b;
	size_t offset;
	__host__ __device__
		prg(float _a = 0.f, float _b = 1.f, size_t offset_ = 0) : a(_a), b(_b), offset(offset_) {};

	__host__ __device__
		float operator()(const unsigned int n) const
	{
		thrust::default_random_engine rng;
		thrust::uniform_real_distribution<float> dist(a, b);
		rng.discard(n + offset);

		return dist(rng);
	}
};

struct L2_dist
{
	int dims;
	// Input is a pointer to the beginning of a row in the input matrix
	float* input0;
	__host__ __device__ L2_dist(int dims_, float* input0_) : dims(dims_), input0(input0_){}

	__host__ __device__
		float operator()(float& data1) const
	{
		float* input1 = &data1;
		float dist = 0;
		float diff;
		for (int i = 0; i < dims; ++i)
		{
			diff = (input1[i] - input0[i]);
			dist += diff*diff;
		}
		return dist;
	}
};


int main()
{
	int dataPoints = 100;
	cv::cuda::Stream stream;
	cv::cuda::GpuMat d_data(5000, 3, CV_32F);
	cv::cuda::GpuMat d_input = cv::cuda::createContinuous(dataPoints, 4, CV_32F);

	thrust::transform(thrust::system::cuda::par.on(cv::cuda::StreamAccessor::getStream(stream)), thrust::counting_iterator<int>(0), thrust::counting_iterator<int>(d_data.rows * 3), GpuMatBeginItr<float>(d_data), prg(-1, 1, clock()));
	flann::Matrix<float> flann_data(d_data.ptr<float>(0), d_data.rows, d_data.cols, d_data.step);

	flann::KDTreeCuda3dIndexParams params;
	params["input_is_gpu_float4"] = true;

	auto oldIndex = flann::GpuIndex<flann::L2<float>>(flann_data, params);
	oldIndex.buildIndex();
	cv::cuda::GpuMat d_idx0 = cv::cuda::createContinuous(dataPoints, 1, CV_32S);
	cv::cuda::GpuMat d_dist0 = cv::cuda::createContinuous(dataPoints, 1, CV_32F);
	cv::cuda::GpuMat d_diff0 = cv::cuda::createContinuous(dataPoints, 3, CV_32F);

	auto newIndex = flann::DynGpuIndex<flann::L2<float>>(flann_data, params);
	newIndex.buildIndex();
	cv::cuda::GpuMat d_diff1 = cv::cuda::createContinuous(dataPoints, 3, CV_32F);
	cv::cuda::GpuMat d_idx1 = cv::cuda::createContinuous(dataPoints, 1, CV_32S);
	cv::cuda::GpuMat d_dist1 = cv::cuda::createContinuous(dataPoints, 1, CV_32F);

	cv::cuda::GpuMat d_diff = cv::cuda::createContinuous(dataPoints, 3, CV_32F);
	cv::cuda::GpuMat d_bf_dist = cv::cuda::createContinuous(d_data.rows, dataPoints, CV_32F);
	cv::cuda::GpuMat d_min, d_max, d_loc;


	cv::cuda::HostMem h_idx0, h_idx1, h_dist0, h_dist1, h_input, h_diff, h_diff0, h_diff1, h_bf_dist;
	flann::Matrix<int> flann_idx0(d_idx0.ptr<int>(), d_idx0.rows, d_idx0.cols, d_idx0.step);
	flann::cuda::DeviceMatrix<int> flann_idx1(d_idx1.ptr<int>(), d_idx1.rows, d_idx1.cols, d_idx1.step);

	flann::Matrix<float> flann_dist0(d_dist0.ptr<float>(), d_dist0.rows, d_dist0.cols, d_dist0.step);
	flann::cuda::DeviceMatrix<float> flann_dist1(d_dist1.ptr<float>(), d_dist1.rows, d_dist1.cols, d_dist1.step);

	flann::Matrix<float> queries(d_input.ptr<float>(), d_input.rows, 3, d_input.step);
	flann::SearchParams searchParams;
	searchParams.matrices_in_gpu_ram = true;
	cv::Mat h_data(d_data);
	for (int i = 0; i < 10; ++i)
	{
		d_input.setTo(cv::Scalar(0), stream);
		d_idx1.setTo(cv::Scalar(0), stream);
		d_idx0.setTo(cv::Scalar(0), stream);
		d_dist1.setTo(cv::Scalar(0), stream);
		d_dist0.setTo(cv::Scalar(0), stream);

		thrust::transform(thrust::cuda::par.on(cv::cuda::StreamAccessor::getStream(stream)), thrust::counting_iterator<int>(0), thrust::counting_iterator<int>(dataPoints * 3), GpuMatBeginItr<float>(d_input.colRange(0, 3)), prg(-1, 1, clock()));

		oldIndex.knnSearch(queries, flann_idx0, flann_dist0, 1, searchParams, cv::cuda::StreamAccessor::getStream(stream));
		newIndex.knnSearch(queries, flann_idx1, flann_dist1, 1, searchParams, cv::cuda::StreamAccessor::getStream(stream));

		for (int i = 0; i < 3; ++i)
		{
			auto diffBegin0 = thrust::make_permutation_iterator(GpuMatBeginItr<float>(d_data.col(i)), GpuMatBeginItr<int>(d_idx0));
			auto diffEnd0 = thrust::make_permutation_iterator(GpuMatEndItr<float>(d_data.col(i)), GpuMatEndItr<int>(d_idx0));

			auto diffBegin1 = thrust::make_permutation_iterator(GpuMatBeginItr<float>(d_data.col(i)), GpuMatBeginItr<int>(d_idx1));
			auto diffEnd1 = thrust::make_permutation_iterator(GpuMatEndItr<float>(d_data.col(i)), GpuMatEndItr<int>(d_idx1));

			auto input = GpuMatBeginItr<float>(d_data.col(i));

			thrust::transform(thrust::cuda::par.on(cv::cuda::StreamAccessor::getStream(stream)), diffBegin0, diffEnd0, diffBegin1, GpuMatBeginItr<float>(d_diff.col(i)), thrust::minus<float>());
			thrust::transform(thrust::cuda::par.on(cv::cuda::StreamAccessor::getStream(stream)), diffBegin0, diffEnd0, input, GpuMatBeginItr<float>(d_diff0.col(i)), thrust::minus<float>());
			thrust::transform(thrust::cuda::par.on(cv::cuda::StreamAccessor::getStream(stream)), diffBegin1, diffEnd1, input, GpuMatBeginItr<float>(d_diff1.col(i)), thrust::minus<float>());
		}
		std::vector<int> minIdx;
		std::vector<float> minValue;
		
		for (int i = 0; i < d_input.rows; ++i)
		{
			// Brute force distance calculation
			thrust::transform(thrust::cuda::par.on(cv::cuda::StreamAccessor::getStream(stream)), GpuMatBeginItr<float>(d_data.col(0)), GpuMatEndItr<float>(d_data.col(0)), GpuMatBeginItr<float>(d_bf_dist.col(i)), L2_dist(d_input.cols, d_input.ptr<float>(i)));
			// Find the min distance
			cv::cuda::findMinMaxLoc(d_bf_dist.col(i), d_min, d_loc, cv::noArray(), stream);
			//auto min_element = thrust::min_element(thrust::cuda::par.on(cv::cuda::StreamAccessor::getStream(stream)), GpuMatBeginItr<float>(d_bf_dist.col(i)), GpuMatEndItr<float>(d_bf_dist.col(i)));
			//minIdx.push_back(min_element - GpuMatBeginItr<float>(d_bf_dist.col(i)));
			//minValue.push_back(*min_element);
		}
		d_input.download(h_input, stream);
		d_idx1.download(h_idx1, stream);
		d_idx0.download(h_idx0, stream);
		d_dist0.download(h_dist0, stream);
		d_dist1.download(h_dist1, stream);
		d_diff.download(h_diff, stream);
		d_diff0.download(h_diff0, stream);
		d_diff1.download(h_diff1, stream);

		cv::Mat idx0 = h_idx0.createMatHeader();
		cv::Mat idx1 = h_idx1.createMatHeader();
		cv::Mat dist0 = h_dist0.createMatHeader();
		cv::Mat dist1 = h_dist1.createMatHeader();
		cv::Mat input = h_input.createMatHeader();
		cv::Mat diff = h_diff.createMatHeader();
		cv::Mat diff0 = h_diff0.createMatHeader();
		cv::Mat diff1 = h_diff1.createMatHeader();
		stream.waitForCompletion();
	}
	


	return 0;
}