#include <thrust/execution_policy.h>
#include <thrust/system/cuda/execution_policy.h>
#include <thrust/reduce.h>
#include <thrust/sequence.h>
#include <thrust/extrema.h>
#include <thrust/device_vector.h>

//#include <bulk/bulk.hpp>
#include <opencv2/core/cuda.hpp>
#include <opencv2/core/cuda_stream_accessor.hpp>
#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudev.hpp>
#include <opencv2/highgui.hpp>

#include <curand.h>
#include <time.h>
#include <memory>

#include "CudaUtils.hpp"
#include "Helpers.hpp"
#include "Defines.hpp"
#include "Mutator.cuh"
#include "Distance.cuh"


#define DISPLAY 0
#define FLANN_USE_CUDA
#include <flann/flann.hpp>
#include <flann/util/DeviceMatrix.h>
#include <flann/algorithms/kdtree_cuda_index.cuh>

/*
TODO
1) Add FLANN update metric
2) More different distance metrics
3) Expand FLANN to D dimensions




*/


template<typename T> void randInit(curandGenerator_t& prng, cv::cuda::GpuMat& node_weights)
{
	curandGenerate(prng, (unsigned int*)node_weights.data, node_weights.cols*node_weights.rows*node_weights.channels()*sizeof(T)/sizeof(unsigned int));	
}

template<> void randInit<float>(curandGenerator_t& prng, cv::cuda::GpuMat& node_weights)
{
	curandGenerateUniform(prng, (float*)node_weights.data, node_weights.rows*node_weights.cols*node_weights.channels());
}

template<> void randInit<double>(curandGenerator_t& prng, cv::cuda::GpuMat& node_weights)
{
	curandGenerateUniformDouble(prng, (double*)node_weights.data, node_weights.rows*node_weights.cols*node_weights.channels());
}

template<typename T>
void randomInit(cv::cuda::GpuMat node_weights)
{
	curandGenerator_t prng;
	curandCreateGenerator(&prng, CURAND_RNG_PSEUDO_DEFAULT);
	curandSetPseudoRandomGeneratorSeed(prng, (unsigned long long) clock());
	randInit<T>(prng, node_weights);
	curandDestroyGenerator(prng);
}

template<typename T> void accumulated_change_download_callback(int status, void* user_data);

template<typename T>
class SOM
{
	EagleLib::BufferPool<cv::cuda::GpuMat> BMU_val_pool;
	EagleLib::BufferPool<cv::cuda::GpuMat> BMU_idx_pool;
	std::shared_ptr<Mutator> mutator;
public:
	SOM(som_idx_type size_, int dims):
		size(size_), rebuilding(false), rebuildNeeded(false)
	{
		accumulated_change.create(1, 1, CV_TYPE<T>::DEPTH);
		accumulated_change.setTo(cv::Scalar(0.0));
		flannIndex.data = nullptr;
		size_t num_nodes = size.x*size.y*size.z*size.w;
		if (dims < 4)
			dims = 4;
		node_weights.data = cv::cuda::createContinuous(num_nodes, dims, CV_TYPE<T>::DEPTH);
		mutator.reset(new Mutator_impl < T, default_mutate_op<T, dist_L2>>(default_mutate_op<T, dist_L2>(0.1) ));
		mutator->SetNeighborhoodSize(5);
		mutator->SetSomSize(size);
		randomInit<T>(node_weights);
		BMU_idx_pool.resize(40);
		BMU_val_pool.resize(40);
		
	}
	bool GetSom(cv::cuda::GpuMat& output)
	{
		if (size.w == 1 && size.z == 1)
		{
			output = node_weights.data.reshape(4, size.y);
			return true;
		}
		return false;
	}
	void BuildIndex(cv::cuda::GpuMat mat, cv::cuda::Stream stream = cv::cuda::Stream::Null())
	{
		std::cout << "(Re)building index\n";
		flann::Matrix<T> matrix_gpu((T*)mat.data, mat.rows, 3, mat.step);
		flann::KDTreeCuda3dIndexParams params;
		params["input_is_gpu_float4"] = true;
		//auto newIndex = new flann::GpuIndex<flann::L2<T>>(matrix_gpu, params);
		auto newIndex = new flann::DynGpuIndex<flann::L2<T>>(matrix_gpu, params);
		
		
		newIndex->buildIndex();
		auto oldIndex = flannIndex.data;
		{
			boost::recursive_mutex::scoped_lock lock(flannIndex.mtx);
			flannIndex.data = newIndex;
		}
		if (oldIndex != nullptr)
		{
			delete oldIndex;
		}
		std::cout << "BuildIndex complete\n";
	}
	void BuildIndex()
	{
		BuildIndex(node_weights);
	}
	void Input(cv::cuda::GpuMat& input, cv::cuda::Stream stream)
	{
		if (rebuildNeeded)
		{
			//buildThreaded();
			//BuildIndex();
		}
		if (node_weights.data.cols == 4)
		{
			CV_Assert(input.cols == 3);
		}
		auto BMU_val = BMU_val_pool.getFront();
		auto BMU_idx = BMU_idx_pool.getFront();
		if (BMU_val->data.rows != input.rows || BMU_val->data.cols != 2 || BMU_val->data.depth() == CV_TYPE<T>::DEPTH)
			BMU_val->data = cv::cuda::createContinuous(input.rows, 2, CV_TYPE<T>::DEPTH);
		if (BMU_idx->data.rows != input.rows || BMU_idx->data.cols != 2 || BMU_idx->data.depth() == CV_TYPE<int>::DEPTH)
			BMU_idx->data = cv::cuda::createContinuous(input.rows, 2, CV_32S);

		BMU_idx->data.setTo(cv::Scalar(-1), stream);
		if (node_weights.data.cols <= 4)
		{
			if (flannIndex.data == nullptr)
			{
				BuildIndex();
			}

			flann::cuda::DeviceMatrix<int> d_idx((int*)BMU_idx->data.data, input.rows, 2, BMU_idx->data.step);
			flann::cuda::DeviceMatrix<float> d_dist((float*)BMU_val->data.data, input.rows, 2, BMU_val->data.step);
			flann::SearchParams searchParams;
			searchParams.matrices_in_gpu_ram = true;
			flann::cuda::DeviceMatrix<T> queries_gpu((T*)input.data, input.rows, input.cols, input.step);
			{
				boost::recursive_mutex::scoped_lock lock(flannIndex.mtx);
				try
				{
					flannIndex.data->knnSearch(queries_gpu, d_idx, d_dist, 1, searchParams, cv::cuda::StreamAccessor::getStream(stream));
				}
				catch (thrust::system_error& error)
				{
					std::cout << error.what() << std::endl;
					return;
				}
			}
			//cv::Mat test(BMU_idx->data);
			CV_CUDEV_SAFE_CALL(cudaGetLastError());
			//stream.waitEvent(node_weights.fillEvent); // Use this essentially as a mutex on the node_weights
			//std::cout << "Input waitEvent\n";
			//stream.waitForCompletion();
			mutator->Mutate(node_weights.data, BMU_idx->data, input,accumulated_change, stream);
			//node_weights.record(stream);
			//std::cout << "Input recordEvent\n";
			accumulated_change.download(h_accumulated_change, stream);
			stream.enqueueHostCallback(accumulated_change_download_callback<T>, this);
			CV_CUDEV_SAFE_CALL(cudaGetLastError());
		}
		else
		{

		}		
	}
	cv::cuda::HostMem h_accumulated_change;
	
	void buildThreaded()
	{
		if (rebuilding)
			return;
		rebuilding = true;
		buildThread_.join();
		buildThread_ = boost::thread(boost::bind(&SOM<T>::buildThread, this));
	}
	bool rebuildNeeded;
private:
	void buildThread()
	{
		
		//tmp_node_weights = cv::cuda::createContinuous(node_weights.data.rows, node_weights.data.cols, node_weights.data.depth());
		if (tmp_node_weights.size() != node_weights.data.size() || tmp_node_weights.depth() != node_weights.data.depth())
		{
			tmp_node_weights = cv::cuda::createContinuous(node_weights.data.rows, node_weights.data.cols, node_weights.data.depth());
		}
		//static cv::cuda::Stream stream;
		// This effectively acts as a mutex on other cuda streams while this data copy occurs
		//buildStream.waitEvent(node_weights.fillEvent); // Wait for node_weights to be free
		//std::cout << "buildThread waitEvent 1\n";
		node_weights.data.copyTo(tmp_node_weights, buildStream); // Launch an async copy
		//node_weights.record(buildStream); // Record event on this stream, so when the other stream checks, it will wait for the copy to be done
		//std::cout << "buildThread recordEvent 1\n";
		buildStream.waitForCompletion();// Wait for the copy to finish
		BuildIndex(tmp_node_weights); 

		//buildStream.waitEvent(node_weights.fillEvent);
		accumulated_change.setTo(cv::Scalar(0.0), cv::noArray(), buildStream);
		node_weights.record(buildStream);
		
		rebuilding = false;
		buildStream.waitForCompletion();
	}
	bool rebuilding;
	
	cv::cuda::GpuMat accumulated_change;
	EagleLib::Buffer<cv::cuda::GpuMat, EagleLib::EventPolicy> node_weights;
	//cv::cuda::GpuMat node_weights;
	som_idx_type size;
	//EagleLib::Buffer<flann::GpuIndex<flann::L2<T>>*, EagleLib::LockedPolicy> flannIndex;
	EagleLib::Buffer<flann::DynGpuIndex<flann::L2<T>>*, EagleLib::LockedPolicy> flannIndex;
	boost::thread buildThread_;
	cv::cuda::Stream buildStream;
	cv::cuda::GpuMat tmp_node_weights;
};
template<typename T> void accumulated_change_download_callback(int status, void* user_data)
{
	SOM<T>* som = static_cast<SOM<T>*>(user_data);
	T change = *(T*)(som->h_accumulated_change.data);
	//std::cout << "Change: " << change << "\n";
	if (abs(change) > 200 || std::isnan(change))
	{
		//som->buildThreaded();
		som->rebuildNeeded = true;
	}
}

// Used for returning an offset from the start of a matrix when asked for an item
struct step_functor : public thrust::unary_function<int, int>
{
	int columns;
	int step;
	step_functor(int columns_, int step_) : columns(columns_), step(step_)	{	};
	__host__ __device__
		int operator()(int x) const
	{
		int row = x / columns;
		int idx = (row * step) + x % columns;
		return idx;
	}
};

template<typename T>
thrust::permutation_iterator<thrust::device_ptr<T>, thrust::transform_iterator<step_functor, thrust::counting_iterator<int>>>  GpuMatBeginItr(cv::cuda::GpuMat mat)
{
	return thrust::make_permutation_iterator(thrust::device_pointer_cast(mat.ptr<T>(0)),
		thrust::make_transform_iterator(thrust::make_counting_iterator(0), step_functor(mat.cols, mat.step / sizeof(T))));
}

template<typename T>
thrust::permutation_iterator<thrust::device_ptr<T>, thrust::transform_iterator<step_functor, thrust::counting_iterator<int>>>  GpuMatEndItr(cv::cuda::GpuMat mat)
{
	return thrust::make_permutation_iterator(thrust::device_pointer_cast(mat.ptr<T>(0)),
		thrust::make_transform_iterator(thrust::make_counting_iterator(mat.rows*mat.cols), step_functor(mat.cols, mat.step / sizeof(T))));
}
struct test
{
	__device__ __host__ void operator()(thrust::tuple<int, int&>& data) const
	{
		printf("%d:%llu\n", thrust::get<0>(data), (void*)&thrust::get<1>(data));
	}
};
int main()
{
	cudaDeviceProp prop;
	int device;
	cudaGetDevice(&device);
	cudaGetDeviceProperties(&prop, device);
	std::cout << "Current deivce" << prop.name << std::endl;
	int count;
	cudaGetDeviceCount(&count);
	for (int i = 0; i < count; ++i)
	{
		cudaGetDeviceProperties(&prop, i);
		std::cout << "Device " << i << " " << prop.name << std::endl;
	}
	int width = 1024;
	int height = 1024;
	som_idx_type size;
	size.x = width;
	size.y = height;
	size.w = 1;
	size.z = 1;
	{// test flann device matrix iterator
		cv::cuda::GpuMat d_mat(100, 100, CV_32F);
		d_mat.setTo(cv::Scalar(0.0));
		flann::cuda::DeviceMatrix<float> flann_mat((float*)(d_mat.data), d_mat.rows, d_mat.cols, d_mat.step);
		thrust::sequence(flann_mat.begin(0), flann_mat.end(0));
		thrust::sequence(flann_mat.begin(3), flann_mat.end(3));
		cv::Mat h_mat(d_mat);
	}
	cv::cuda::Stream stream;
	SOM<float> som(size, 3);
	cv::cuda::GpuMat d_mat, d_prevSom, diff;
	int frame = 0;
#if DISPLAY
	EagleLib::BufferPool<cv::cuda::GpuMat> displayBufferPool;
	auto displayBuffer = displayBufferPool.getFront();
	som.GetSom(d_mat);
	cv::cuda::GpuMat merged;
	std::vector<cv::cuda::GpuMat> normalized, channels;
	cv::cuda::split(d_mat, channels, stream);
	normalized.resize(channels.size());
	for (int i = 0; i < channels.size(); ++i)
	{
		cv::cuda::normalize(channels[i], normalized[i], 0, 255, cv::NORM_MINMAX, CV_8UC1, cv::noArray(), stream);
	}
	{
		std::vector<cv::cuda::GpuMat> toMerge(normalized.begin(), normalized.begin() + 3);
		cv::cuda::merge(toMerge, merged, stream);
	}
	merged.copyTo(displayBuffer->data, stream);
		
	cv::namedWindow("Original", cv::WINDOW_OPENGL);
	cv::namedWindow("Current", cv::WINDOW_OPENGL);
	cv::imshow("Original", displayBuffer->data);
#endif
	cv::cuda::HostMem input(10000, 3, CV_32F);
	//cv::cuda::GpuMat d_input;
	EagleLib::BufferPool<cv::cuda::GpuMat, EagleLib::EventPolicy> inputPool;
	inputPool.resize(40);
	int key = 0;
	auto time = clock();
	int itr = 0;
	auto prevDisplay = clock();
	while (key != 27)
	{
		
		auto d_input = inputPool.getFront();
		cv::randn(input, cv::Scalar(.5), cv::Scalar(.25));
		d_input->data.upload(input, stream);
		som.Input(d_input->data, stream);
		auto curTime = clock();
		//std::cout << curTime - time << std::endl;
		std::cout << "Frame num: " << ++frame << " frame time: " << curTime - time << std::endl;
		time = curTime;
		++itr;
#if DISPLAY
		if (clock() - prevDisplay > 100)
		{
			prevDisplay = clock();
			som.GetSom(d_mat);
			cv::cuda::split(d_mat, channels, stream);
			normalized.resize(channels.size());
			displayBuffer = displayBufferPool.getFront();
			for (int i = 0; i < channels.size(); ++i)
			{
				cv::cuda::normalize(channels[i], normalized[i], 0, 255, cv::NORM_MINMAX, CV_8UC1, cv::noArray(), stream);
			}
			{
				std::vector<cv::cuda::GpuMat> toMerge(normalized.begin(), normalized.begin() + 3);
				cv::cuda::merge(toMerge, merged, stream);
			}
			displayBuffer->wait();
			merged.copyTo(displayBuffer->data, stream);
			displayBuffer->record(stream);
			cv::imshow("Current", displayBuffer->data);
			key = cv::waitKey(1);
		}
		
		
#endif
	}
	stream.waitForCompletion();

	
	return 0;
}