#include "svd/svd.h"

int main()
{
	cv::cuda::Stream stream;
	cv::cuda::GpuMat X = cv::cuda::createContinuous(100, 100, CV_32F);
	cv::cuda::GpuMat C = cv::cuda::createContinuous(100, 5, CV_32F);
	iterative_svd svd;
	cv::cuda::GpuMat U, V, S, U_, V_, S_, U__, V__, S__; // u,v,s, u', v', s', u'', v'', s''

	svd.initialize(X, U, V, S, stream);
	svd.addCols(C, U_, V_, S_, stream);

	cv::cuda::GpuMat concatenated;
	cv::cuda::hconcat(X, C, concatenated, -1, stream);
	cv::cuda::SVD(concatenated, U__, V__, S__, stream);
	stream.waitForCompletion();
	cv::Mat h_U(U);
	cv::Mat h_V(V);
	cv::Mat h_S(S);
	cv::Mat h_U_(U_);
	cv::Mat h_V_(V_);
	cv::Mat h_S_(S_);
	cv::Mat h_U__(U__);
	cv::Mat h_V__(V__);
	cv::Mat h_S__(S__);



	return 0;
}