#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>

#include<iostream>
#include<stdlib.h>
#include<stdio.h>
#include <cusolverDn.h>
#include <cuda_runtime_api.h>

#define HAVE_CUDA
#include <opencv2/core/cuda.hpp>
#include <opencv2/core/cuda_stream_accessor.hpp>

#include "Thrust_interop.hpp"
#include <thrust/random.h>
#include <thrust/transform.h>
#include "prg.cuh"
#include <time.h>
#include "pcap.h"
#include <deque>
#include "svd/svd.h"
#define PACKET_LEN 2048
//#include "magma.h"

//#include <opencv2/core/private.cuda.hpp>

/***********************/
/* CUDA ERROR CHECKING */
/***********************/
void gpuAssert(cudaError_t code, char *file, int line, bool abort = true)
{
	if (code != cudaSuccess)
	{
		fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
		if (abort) { exit(code); }
	}
}
void gpuErrchk(cudaError_t ans) { gpuAssert((ans), __FILE__, __LINE__); }








/********/
/* MAIN */
/********/
int main(){
	//magma_init();
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t *fp;

	fp = pcap_open_offline("outside.tcpdump", errbuf);
	if (fp == 0)
	{
		std::cout << "Unable to open tcpdump file\n";
		return -1;
	}
	struct pcap_pkthdr *header;
	const u_char *pkt_data;
	int res;
	u_int i = 0;
	std::deque<pcap_pkthdr*> headers;
	std::deque<const u_char*> payloads;
	int maxLen = 0;
	int count = 0;
	while ((res = pcap_next_ex(fp, &header, &pkt_data)) >= 0 && count < 3000)
	{
		headers.push_back(header);
		payloads.push_back(pkt_data);

		/* print pkt timestamp and pkt len */
		//printf("%ld:%ld (%ld)\n", header->ts.tv_sec, header->ts.tv_usec, header->len);
		if (header->caplen > maxLen)
			maxLen = header->caplen;
		/* Print the packet */
		for (i = 1; (i < header->caplen + 1); i++)
		{
			//printf("%.2x ", pkt_data[i - 1]);
			if ((i % 16) == 0) printf("\n");
		}

		//printf("\n\n");
		++count;
	}
	std::cout << "Max length: " << maxLen << "\n";
	
	//cv::cuda::GpuMat h_data = cv::cuda::createContinuous(headers.size(), PACKET_LEN, CV_32F);
	cv::Mat h_data(count, PACKET_LEN, CV_32F);
	h_data.setTo(cv::Scalar(0));
	auto hdrItr = headers.begin();
	auto payItr = payloads.begin();
	int row = 0;
	for (; hdrItr != headers.end() && payItr != payloads.end(); ++hdrItr, ++payItr, ++row)
	{
		for (int i = 0; i < (*hdrItr)->caplen; ++i)
		{
			h_data.at<float>(row, i) = (*payItr)[i];
		}
	}

	cv::cuda::GpuMat d_data(h_data);
	cv::cuda::GpuMat Q, R;
	cv::cuda::QR(d_data, Q, &R);
	//auto begin = GpuMatBeginItr<float>(d_data);
	//auto end = GpuMatEndItr<float>(d_data);
	//thrust::transform(begin, end, begin, prg(-100, 100, clock()));
	std::cout << "Done filling a matrix of size " << d_data.size() << std::endl;


	if (false)
	{
		std::cout << "Running OpenCV SVD\n";
		//cv::Mat h_data(d_data);
		cv::SVD svd;
		cv::Mat cpu_W, cpu_U, cpu_V;
		auto startCPU = clock();
		svd.compute(h_data, cpu_W, cpu_U, cpu_V, cv::SVD::NO_UV);
		std::cout << "CPU SVD took " << clock() - startCPU << " ms\n";
		cv::FileStorage fs("SVD_results_cpu.yml", cv::FileStorage::WRITE);
		fs << "U" << cpu_U;
		fs << "V" << cpu_V;
		fs << "W" << cpu_W;
	}
	
	if (true)
	{
		std::cout << "Running cuSolver SVD\n";
		cv::cuda::GpuMat d_U, d_V, d_S;
		cv::cuda::Stream stream;
		auto startTime = clock();
		cv::cuda::SVD(d_data, d_U, d_V, d_S, stream);
		std::cout << "End time " << clock() << "\n";
		stream.waitForCompletion();
		auto endTIme = clock();
		std::cout << "GPU SVD took " << endTIme - startTime << " ms\n";
		cv::cuda::HostMem h_U, h_V, h_S;
		d_U.download(h_U, stream);
		d_V.download(h_V, stream);
		d_S.download(h_S, stream);

		auto U = h_U.createMatHeader();
		auto V = h_V.createMatHeader();
		auto S = h_S.createMatHeader();

		stream.waitForCompletion();
		{
			cv::FileStorage fs("SVD_result_U.yml", cv::FileStorage::WRITE);
			fs << "U" << U;
		}
		{
			cv::FileStorage fs("SVD_result_V.yml", cv::FileStorage::WRITE);
			fs << "V" << V;
		}
		{
			cv::FileStorage fs("SVD_result_S.yml", cv::FileStorage::WRITE);
			fs << "S" << S;
		}
		
		
	}
	

	if (false)
	{
		std::cout << "Running SVD via magma\n";
		int M = d_data.rows;
		int N = d_data.cols;
		cv::Mat _U, _V, _S;
		_U = cv::Mat(M, M, CV_32F);
		_V = cv::Mat(N, N, CV_32F);
		_S = cv::Mat(1, N, CV_32F);
		cv::Mat h_data(d_data);
		cv::Mat workBuffer;
		int info;
		int work_size;
		cusolverDnHandle_t solver_handle;
		cusolverDnCreate(&solver_handle);
		cusolverDnSgesvd_bufferSize(solver_handle, M, N, &work_size);
		workBuffer = cv::Mat(1, work_size, CV_32F);
		auto magma_start = clock();
		/*
			Currently doesn't work, seems to be throwing an error about the LDA even though it's the same LDA used with the cusolver API.
		*/
		//magma_sgesvd(MagmaAllVec, MagmaAllVec, M, N, h_data.ptr<float>(), M, _S.ptr<float>(), _U.ptr<float>(), M, _V.ptr<float>(), N, workBuffer.ptr<float>(), work_size, &info);
		//std::cout << "Magma svd completed in " << clock() - magma_start << " ms\n";
	}




	return 0;
}